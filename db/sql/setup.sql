CREATE DATABASE IF NOT EXISTS ticker_data;
USE ticker_data;

# The exchange table contains information (the name and currency-traded) of the exchange. This is a parent of table security.
CREATE TABLE IF NOT EXISTS exchanges (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(10) NOT NULL,
    `currency` CHAR(3) NULL DEFAULT NULL,
    `created_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
    `last_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
    PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

# Insert exchanges that are supported by this platform
INSERT INTO exchanges (name, currency) VALUES ('NASDAQ', 'USD');
INSERT INTO exchanges (name, currency) VALUES ('NYSE', 'USD');
INSERT INTO exchanges (name, currency) VALUES ('AMEX', 'USD');


# security holds all of the relevant information about the actual companies for which we will collect data.
# Actually the only thing we really need here is the ticker (and which exchange it is traded on),
# but I thought it might be nice to have some other data like the sector and industry.
CREATE TABLE IF NOT EXISTS `securities` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `exchange_id` INT(11) NOT NULL,
    `ticker` VARCHAR(10) NOT NULL,
    `name` VARCHAR(500) NULL,
    `sector` VARCHAR(500) NULL,
    `industry` VARCHAR(500) NULL,
    `created_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
    `last_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
    PRIMARY KEY (`id`),
    INDEX `exchange_id` (`exchange_id` ASC),
    INDEX `ticker` (`ticker` ASC),
    CONSTRAINT `fk_exchange_id`
        FOREIGN KEY (`exchange_id`)
        REFERENCES exchanges (`id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

# Vendors to get data from, ex. Yahoo Finance
CREATE TABLE IF NOT EXISTS data_vendors (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `website_url` VARCHAR(255) NULL DEFAULT NULL,
    `created_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
    `last_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
    PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO data_vendors (name, website_url) VALUES ('YahooFinance', 'https://finance.yahoo.com');
INSERT INTO data_vendors (name, website_url) VALUES ('Nasdaq', 'https://api.nasdaq.com');
INSERT INTO data_vendors (name, website_url) VALUES ('Finnhub', 'https://finnhub.io/');

# Okay lets actually store the prices now
CREATE TABLE IF NOT EXISTS `daily_prices` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `data_vendor_id` INT(11) NOT NULL,
    `ticker_id` INT(11) NOT NULL,
    `price_date` DATE NOT NULL,
    `created_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
    `last_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
    `open_price` DECIMAL(11,6) NULL DEFAULT NULL,
    `high_price` DECIMAL(11,6) NULL DEFAULT NULL,
    `low_price` DECIMAL(11,6) NULL DEFAULT NULL,
    `close_price` DECIMAL(11,6) NULL DEFAULT NULL,
    `adj_close_price` DECIMAL(11,6) NULL DEFAULT NULL,
    `volume` BIGINT(20) NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `price_date` (`price_date` ASC),
    INDEX `ticker_id` (`ticker_id` ASC),
    CONSTRAINT `fk_ticker_id`
        FOREIGN KEY (`ticker_id`)
        REFERENCES `securities` (`id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT `fk_data_vendor_id`
        FOREIGN KEY (`data_vendor_id`)
        REFERENCES data_vendors (`id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;